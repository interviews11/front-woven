import './App.css';
import { useState } from 'react'
import CreateAccountForm from './CreateAccountForm';
import { generateFakeData } from './fakeData';
import IsUserOffline from './isUserOffline';
import { clearData, createBulk, getData, table } from './database';
import AccountInfo from './AccountInfo';
import { dropRecord, getNewAccount, listen } from './socket';

function LocalStorageApp() {
  const [form, setForm] = useState(false)
  const [createAt, setCreateAt] = useState('0')
  const [sortedIn, setSortedIn] = useState('0')
  const [displayAt, setDisplayAt] = useState('0')
  const [numberOfData, setNumberOfData] = useState(0)

  const [dbData, setDbData] = useState(null)
  const [account, newAccount] = useState([])
  const [isLoading, setIsLoading] = useState(false)
  const generateStyle = {
    maxWidth: "500px",
    margin: "0 auto"
  }
  const generateData = () => {
    setIsLoading(true)
    const size = 100 * 100;
    const data = generateFakeData(size);
    setNumberOfData(size)
    createBulk(data, (time, data) => {
      setCreateAt(time)
      setIsLoading(false)
    }, true)
  }

  const viewData = () => {
    setIsLoading(true)
    getData((time, data) => {
      setDisplayAt(time)
      setDbData(data)
      setIsLoading(false)

    }, true)
  }
  setTimeout(() => {
    listen('DUMP_TRANSACTION', (data) => {
      newAccount([ ...data, ...account,])
    })
  })
  const clearDatabase = () => {
    clearData();
  }
  return (
    <div className="App">
      <IsUserOffline />
      <header className="title App-header">
          <h1>LocalStorage App</h1>
      </header>
    {
      isLoading && <div className="loader"></div>
    }

    <div className="timestamp"> 
            <div className="created">
                <span> Created In </span>
                {createAt}
            </div>
            <div>
                <span> SortedIn </span>
                {sortedIn}
            </div>
            <div className="display">
                <span> Displayed withIn </span>
                {displayAt}
            </div>
    </div>

      <div style={generateStyle}>

      <div>Generated Records: <span>{numberOfData}</span></div>
      <br />
      <div style={{ display: "flex", width: "80%", justifyContent: "space-around"}}>
      <button onClick={generateData} style={{ margin: "10px" }}>generate</button>
      <button onClick={viewData} style={{ margin: "10px", background: "#123" }}>display records</button>
      <button onClick={clearDatabase} style={{ margin: "10px", background: "#db3434" }}>clear records</button>
      </div>
      </div>
         <h2> LocalStorage accounts </h2>

      <div className="profiles">

          {
            dbData ? dbData.map(item => <AccountInfo {...item} />) : <span> Not displaying data yet, click on display </span>
          }
      </div>
      
       { form && <CreateAccountForm onClose={setForm} onCreated={(data) => newAccount([ ...data, ...account,])} /> }
          
            <br />
            <br />
            <br />
              <h2> Mongo Database accounts </h2>
      <div className="profiles">
          {
            account && account.map(item => <AccountInfo {...item} />)
          }
          </div>
        
          <div className="add-profile" onClick={() => setForm(!form)}>{ !form ? "+" : "x"}</div>
    </div>
  );
}

export default LocalStorageApp;
