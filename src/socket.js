import config from './config'
const CREATE_ACCOUNT = 'CREATE_ACCOUNT';
const GET_NEW_ACCOUNT = 'GET_NEW_ACCOUNT';

let baseURL =  config.socket
// const socket = openSocket(baseURL);
const socket = new WebSocket(baseURL);
// Show a connected message when the WebSocket is opened.
socket.onopen = function(event) {
  console.log( 'Connected to: ' + event.currentTarget.url);
};

function send(event, data) {
  return socket.send(JSON.stringify({ event, data}));
}
function formatResponse (data) {
  return JSON.parse(data)
}
socket.onerror = function(error) {
  console.log('WebSocket Error: ' + error);
};
export const listen = (eventName, callback) => {
  socket.onmessage = function(event) {
    const data = formatResponse(event.data);
    console.log(data, eventName)
    if (data.event === eventName) {
      callback(data.data)
    }
    // callback(event)
  }
}

export const getNewAccount = (callback) => {
  try {
  listen(GET_NEW_ACCOUNT, callback)
  } catch (e) {
    console.log(e)
  }
}


export const createAccount = (data, callback) => {
  send(CREATE_ACCOUNT, data);
  listen(CREATE_ACCOUNT, callback)
}
export const dropRecord = (callback) => {
  send('DROP_TABLE', 'as');
  listen('DROP_TABLE', callback)

}
