import Dexie from 'dexie';

const database = new Dexie('woven-finance-db');
database.version(1).stores({
    onlineAccount: `++id,timestamp,name,dob,email,phone,address,bvn,generatedAt`,
    offlineAccount: `++id,timestamp,name,dob,email,phone,address,bvn,generatedAt`,
});
export default {
 backend: 'http://localhost:7003',
 socket: 'ws://localhost:7003',
 database
}