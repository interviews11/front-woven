import React from "react";
import { Route, Switch,  BrowserRouter } from "react-router-dom";
import App from "./App";
import LocalStorageApp from "./LocalStorage";

export default function Routes() {

  return (
    <BrowserRouter>
      <Switch>
        <Route exact path={ ["/", "/indexDB"]} component={App} />
        <Route exact path={ '/:localstoreage' } role="user" component={LocalStorageApp} />
      </Switch>
    </BrowserRouter>
  );
}
