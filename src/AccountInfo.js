import { useState } from "react";

export default function AccountInfo({title,image, name, address, dob, phone, email, className, bvn }) {
  return (
       <div className="profile ${className}">
            <div className="about">
                <div> 
                    <div className="title"> {title || name} </div>
                    <div> {email} | {phone} </div>
                    <br />
                    <div> <strong>BVN:</strong> {bvn} </div>
                    <div> <strong>DOB:</strong> {dob} </div>
                    <div> 
                        {address}
                    </div>
                </div>
            </div>
        </div>
  );
}
