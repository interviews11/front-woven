import faker from 'faker/locale/en'

export const data = {
 name: faker.name.findName(),
 email: faker.internet.email(),
 generatedAt: Date.now(),
 bvn: faker.random.number(9999 * 99991),
 address: faker.address.secondaryAddress(),
 phone: faker.phone.phoneNumber()
}
export const generateFakeData = (size=100) => {
 const data = []
  for(let ind = 0; ind < size; ind++) {
    data.push({
      name: faker.name.findName(),
      email: faker.internet.email(),
      generatedAt: Date.now(),
      bvn: faker.random.number(9999 * 99991),
      address: faker.address.secondaryAddress(),
      phone: faker.phone.phoneNumber()
   })
  }
  return data;
}