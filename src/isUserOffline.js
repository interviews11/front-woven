import { useState } from "react";

export default function IsUserOffline() {
  const [isOffline, setIsOffline] = useState(false)
 if (navigator.onLine && isOffline) {
  setIsOffline(false)
 }
 window.addEventListener('online', function(e) {
    setIsOffline(false)
 }, false);
 
 window.addEventListener('offline', function(e) {
    setIsOffline(true)
}, false);
  return (
    
<div id="offline"className={!isOffline ? 'hide' : ''}>
  <div className="indicator">.</div>
</div>
  );
}
 