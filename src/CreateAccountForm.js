import { useState } from "react";
import { createAccount } from "./socket";

export default function CreateAccountForm({ onCreated, onClose }) {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [bvn, setBvn] = useState("");
  const [address, setAddress] = useState("");
  const [dob, setDob] = useState("");

  const getValue = (callback) => (event) => {
    callback(event.target.value);
  };
  function submitForm() {
    createAccount({
      name,email, phone, bvn,address,dob, timeSent: Date.now()
    }, (data) => {
      if (data.error) {
        alert(data.error)
      } else {
        onCreated(data)
        onClose(false)
      }
      console.log({data})
    })
  }
  return (
    <div className="form-wrapper">
      <form>
        <div className="header"> Create Account </div>
        <div className="content">
          <input
            id="name"
            placeholder="Full name"
            onKeyUp={getValue(setName)}
            onChange={getValue(setName)}
          />
          <input
            id="email"
            placeholder="Email Address"
            onKeyUp={getValue(setEmail)}
            onChange={getValue(setEmail)}
          />
          <input
            id="phone"
            placeholder="Phone number"
            onKeyUp={getValue(setPhone)}
            onChange={getValue(setPhone)}
          />
          <input id="bvn" placeholder="BVN"  onChange={getValue(setBvn)} onKeyUp={getValue(setBvn)} />
          <input
            id="address"
            placeholder="address"
            onKeyUp={getValue(setAddress)}
            onChange={getValue(setAddress)}
          />
          <input
            id="dob"
            placeholder="Date of birth"
            onKeyUp={getValue(setDob)}
            onChange={getValue(setDob)}
          />

          <button type="button" onClick={submitForm}>
            create Account
          </button>
        </div>
      </form>
    </div>
  );
}
