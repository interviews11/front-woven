import config from './config'
export  const table = config.database.onlineAccount;                                                                                      
  
  // table.delete().then(console.log)
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             ;
  export function createBulk(data, callback, useLocalStorage=false) {
   const timeStart = Date.now();
   console.log('started transaction @,', {timeStart})
    if (!useLocalStorage){
   table.bulkPut(data)
     .then((lastKey) => {
       callback ((Date.now() - timeStart) / 1000)
     })
    } else {
      const oldData = JSON.parse(localStorage.getItem('DATABASE') || '[]')
      const newData = localStorage.setItem('DATABASE', JSON.stringify([...data, ...oldData]));
      callback((Date.now() - timeStart) / 1000, newData)
    }
  }

  export function getData(callback, useLocalStorage=false) {
    const displayStart = Date.now();
    if (!useLocalStorage){
      table.toArray().then(records => {
        callback((Date.now() - displayStart) / 1000, records)
      })
    } else {
      const data = JSON.parse(localStorage.getItem('DATABASE') || '[]')
      callback((Date.now() - displayStart) / 1000, data)
    }
  }

  export function clearData() {
    table.clear().catch();
  }